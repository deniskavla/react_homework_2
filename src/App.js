import React from 'react';
import './bootstrap.min.css'
import './App.css';
import Container from "./Components/Container";

class App extends React.Component {
    state = {
        edit: false,
        books: [],
        book: {
            id: 1,
            bookName: '',
            bookAuthor: '',
            bookIsbn: '',
            isVisible: false
        }
    };

    onClickAddBook = (name, value) => {
        this.setState((state) => {
            return {...state, book: {...state.book, [name]: value}}
        })
    }
    onClickEditBook = (id, name, value) => {
        this.setState({
            books: this.state.books.map(books => {
                if (books.id === id) {
                    return {...books, [name]: value}
                }
            })
        })
    }
    s

    onStateBooks = () => {
        this.setState((state) => {
            return {
                books: [...state.books, state.book],
                book: {
                    id: Math.random(),
                    bookName: '',
                    bookAuthor: '',
                    bookIsbn: ''
                }
            }
        });
    }
    deleteBook = (id) => {
        this.setState({
            books: this.state.books.filter(book => book.id !== id)
        })
    }

    editBook = (id) => {
        this.setState({
            books: this.state.books.map(book => {
                if (book.id === id) {
                    return {...book, isVisible: !book.isVisible}
                } else {
                    return book
                }
            })
        })
    }

    render() {
        return (
            <Container state={this.state}
                       onClickAddBook={this.onClickAddBook}
                       onClickEditBook={this.onClickEditBook}
                       onStateBook={this.onStateBooks}
                       deleteBook={this.deleteBook}
                       editBook={this.editBook}/>
        )
    }
}

export default App;