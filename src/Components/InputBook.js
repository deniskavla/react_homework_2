import React from "react";

const InputBook = (props) => {

    const onStateBook = (event) => {
        props.onStateBook(props)
    }
    const handleBook = (event) => {
        const {name, value} = event.target;
        props.onClickAddBook(name, value)
    }

    return (
        <div className="row">
            <div className="col-lg-4">
                <form id="add-book-form">
                    <div className="form-group">
                        <label htmlFor="title">Title</label>
                        <input value={props.book && props.book.bookName}
                               onChange={handleBook}
                               type="text"
                               name="bookName"
                               className="form-control"/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="author">Author</label>
                        <input value={props.book && props.book.bookAuthor}
                               onChange={handleBook}
                               type="text"
                               name="bookAuthor"
                               className="form-control"/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="title">ISBN#</label>
                        <input value={props.book && props.book.bookIsbn}
                               onChange={handleBook}
                               type="text"
                               name="bookIsbn"
                               className="form-control"/>
                    </div>
                    <input onClick={onStateBook} type="text" value="Add Book"
                           className="btn btn-primary"/>
                </form>
            </div>
        </div>
    )
}
export default InputBook;