import React from "react";
import InputBook from "./InputBook";
import BookItem from "./BookItem";
import EditBook from "./EditBook";


const Container = ({state, onClickAddBook, onClickEditBook, onStateBook, deleteBook, editBook}) => {

    return (
        <div className="container mt-4">
            <h1 className="display-4 text-center"><i className="fas fa-book-open text-primary"></i> <span
                className="text-secondary">Book</span> List</h1>
            <p className="text-center">Add your book information to store it in database.</p>

            <InputBook book={state.book} onClickAddBook={onClickAddBook} onStateBook={onStateBook}/>

            <h3 id="book-count" className="book-count mt-5">Всего книг: {state.books.length}</h3>
            <table className="table table-striped mt-2">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Author</th>
                    <th>ISBN#</th>
                    <th>Title</th>
                </tr>
                </thead>
                <tbody id="book-list">

                {state.books.map((book) => {
                    return (
                        !book.isVisible
                            ? <BookItem book={book} key={book.id} deleteBook={deleteBook} editBook={editBook}/>
                            : <EditBook onClickEditBook={onClickEditBook}
                                        book={book}
                                        key={book.id}
                                        deleteBook={deleteBook} e
                                        editBook={editBook}/>
                    )
                })
                }
                </tbody>
            </table>
        </div>
    )
}


export default Container;