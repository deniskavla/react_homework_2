import React from "react";


const EditBook = ({book, editBook, deleteBook, onClickEditBook}) => {

    const handleEditInput = (event) => {
        const {name, value} = event.target;
        onClickEditBook(book.id, name, value)
    }
    return (
        <tr>
            <td>
                <div className="form-group">
                    <input type="text" name="bookName"
                           className="form-control"
                           onChange={handleEditInput}
                           value={book.bookName}/>

                </div>
            </td>
            <td>
                <div className="form-group">
                    <input type="text" name="bookAuthor"
                           className="form-control"
                           onChange={handleEditInput}
                           value={book.bookAuthor}/>
                </div>
            </td>
            <td>
                <div className="form-group">
                    <input type="text" name="bookIsbn"
                           className="form-control"
                           onChange={handleEditInput}
                           value={book.bookIsbn}/>
                </div>
            </td>
            <td onClick={() => editBook(book.id)}>
                <a href="#" className="btn btn-info btn-sm btn-edit">
                    <i className="fas fa-edit"></i>
                </a>
            </td>
            <td onClick={() => deleteBook(book.id)}>
                <a href="#" className="btn btn-danger btn-sm btn-delete">X</a>
            </td>
        </tr>
    )
}

export default EditBook;