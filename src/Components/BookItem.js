import React from "react";


const BookItem = ({book, editBook, deleteBook}) => {

    return (
        <tr>
            <td>{book.bookName}</td>
            <td>{book.bookAuthor}</td>
            <td>{book.bookIsbn}</td>
            <td onClick={() => editBook(book.id)}>
                <a href="#" className="btn btn-info btn-sm">
                    <i className="fas fa-edit"></i>
                </a>
            </td>
            <td onClick={() => deleteBook(book.id)}>
                <a href="#" className="btn btn-danger btn-sm btn-delete">X</a>
            </td>
        </tr>


    )
}
export default BookItem;